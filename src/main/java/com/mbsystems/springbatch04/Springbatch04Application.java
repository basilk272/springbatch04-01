package com.mbsystems.springbatch04;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Springbatch04Application {

    public static void main( String[] args ) {
        SpringApplication.run( Springbatch04Application.class, args );
    }

}
